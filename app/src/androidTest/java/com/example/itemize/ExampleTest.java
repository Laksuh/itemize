package com.example.itemize;

import androidx.test.rule.ActivityTestRule;

import com.mauriciotogneri.greencoffee.GreenCoffeeConfig;
import com.mauriciotogneri.greencoffee.GreenCoffeeTest;
import com.mauriciotogneri.greencoffee.ScenarioConfig;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.IOException;
import java.util.Locale;


@RunWith(Parameterized.class)
public class ExampleTest extends GreenCoffeeTest {
    @Rule
    public ActivityTestRule activity = new ActivityTestRule<>(MainActivity.class);

    public ExampleTest(ScenarioConfig scenario) {
        super(scenario);
    }

    @Parameterized.Parameters(name = "{0}")
    public static Iterable<ScenarioConfig> scenarios() throws IOException{
        return new GreenCoffeeConfig()
                .withFeatureFromAssets("/Users/d0283861/AndroidStudioProjects/GitItemize/itemize/Itemize/app/src/androidTest/java/com/example/itemize/assets/features/example.feature")
                .scenarios(
                        new Locale("en", "DE")
                );
    }

    @Test
    public void test() {
        start(new ItemSteps());
    }
}
