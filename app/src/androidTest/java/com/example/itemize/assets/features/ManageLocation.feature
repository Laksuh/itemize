Feature: List of Locations and Items screen
  In order to see the list of locations
  As a user of the app
  I want to manage the location list

  Scenario: Add a location to the list
    Given I have at least one location added
    When  I press the "+" button
    Then  I can see a DialogBox
    And   I can enter Details
    And   I the "Add" button adds a location

  Scenario: Delete a location from the list
    Given I have at least one location added
    When  I press a location at least 0.5 seconds
    Then  I can see a DialogBox
    And   I can edit the locations Details
    And   I the "Delete" button removes the location

  Scenario: edit a location from the list
    Given I have at least one location added
    When  I press a location at least 0.5 seconds
    Then  I can see a DialogBox
    And   I can edit the locations Details
    And   I the "Edit" button removes the location

  Scenario: User has no location
    Then  I see an empty list