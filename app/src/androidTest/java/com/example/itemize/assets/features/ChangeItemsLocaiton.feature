Feature: List of Items in locations screen
  In order to see the location of my items
  As a user of the app
  I want to manage the location of the items

  Scenario: change the location of an item
    Given I have at least two locations and one item added
    When  I press an item for at least 0.5 seconds
    And   I drag it to another location
    Then  I the item changes its place to the dragged location

  Scenario: Not enough items
    Given I have at least two locations added
    Then  I see two empty locations

  Scenario: Not enough locations
    Given I have at least one item added
    Then  I see a TextView with the text "please add at least two locations"

  Scenario: User has no locations nor an item
    Then  I see a TextView with the text "please add at least two locations"
