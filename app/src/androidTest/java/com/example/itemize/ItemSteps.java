package com.example.itemize;

import com.mauriciotogneri.greencoffee.GreenCoffeeSteps;
import com.mauriciotogneri.greencoffee.annotations.Given;
import com.mauriciotogneri.greencoffee.annotations.When;

public class ItemSteps extends GreenCoffeeSteps {

    @Given("^I see something$")
    public void iSeeSomething(){
        onViewWithId(R.id.nav_view).isDisplayed();
    }

    @When("^I add an item$")
    public void addAnItem(){
        onViewWithId(R.id.fabAddItem).click();
    }
}
