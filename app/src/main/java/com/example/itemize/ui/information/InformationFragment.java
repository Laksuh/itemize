package com.example.itemize.ui.information;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.example.itemize.R;

public class InformationFragment extends Fragment {

    public static InformationFragment newInstance() {
        return new InformationFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_information, container, false);
        InformationViewModel informationViewModel = ViewModelProviders.of(this).get(InformationViewModel.class);
        Button btnBlog = (Button) root.findViewById(R.id.btnBlog);
        Button btnGuide = (Button) root.findViewById(R.id.btnGuide);
        Button btnPN = (Button) root.findViewById(R.id.btnPN);
        btnBlog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToUrl("https://itemizeproject.wordpress.com/");
            }
        });
        btnGuide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToUrl("https://itemizeproject.wordpress.com/user-guide/");
            }
        });
        btnPN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToUrl("https://itemizeproject.wordpress.com/patch-notes/");
            }
        });
        return root;
    }

    private void goToUrl (String url) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        intent.addCategory(Intent.CATEGORY_BROWSABLE);
        intent.setData(Uri.parse(url));
        startActivity(intent);
    }
}
