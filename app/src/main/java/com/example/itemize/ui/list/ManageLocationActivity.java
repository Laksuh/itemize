package com.example.itemize.ui.list;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.example.itemize.ItemAdapter;
import com.example.itemize.Location;
import com.example.itemize.LocationAdapter;
import com.example.itemize.R;
import java.util.ArrayList;

public class ManageLocationActivity extends AppCompatActivity {

    //private ListViewModel listViewModel;
    private LocationAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_location);

        //Init Recyclerview with LinearLayout for the Items + create Items
        final RecyclerView recyclerView;
        recyclerView = findViewById(R.id.rvLocations);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        recyclerView.setHasFixedSize(true);

        ArrayList<Location> locations = Location.createLocationList(getApplicationContext());

        final LocationAdapter adapter = new LocationAdapter(locations, getApplicationContext());
        this.adapter=adapter;
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(layoutManager);
        //end init RecyclerView

        //init button
        //FloatingActionButton addLocation = findViewById(R.id.fabAddLocation);

       /* addLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addLocationDialog(view);
            }
        });*/
        //end init button

        //init LongItemClickListener
        adapter.setOnItemClickListener(new ItemAdapter.ClickListener() {
            @Override
            public void onItemLongClick(final int position, View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());
                final EditText input = new EditText(v.getContext());
                input.setText(LocationAdapter.getmLocations().get(position).getName());
                builder.setView(input);
                builder.setTitle("Edit " + LocationAdapter.getmLocations().get(position).getName());
                builder.setMessage("Rename location:");
                builder.setPositiveButton("Rename", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        adapter.renameLocation(position,input.getText().toString());
                        adapter.notifyDataSetChanged();
                    }
                });
                builder.setNeutralButton("Cancel", null);
                /*builder.setNegativeButton("Delete", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        adapter.removeLocation(position);
                        recyclerView.removeViewAt(position);
                        adapter.notifyItemRemoved(position);
                        adapter.notifyItemRangeChanged(position, adapter.getItemCount());
                        adapter.notifyDataSetChanged();
                    }
                });*/
                builder.create();
                builder.show();
            }
        });
        //end init LongItemClickListener

    }

    public void addLocationDialog(View view){
        AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
        final EditText input = new EditText(view.getContext());
        builder.setView(input);
        builder.setTitle("Add Location");
        builder.setMessage("Location name:");
        builder.setPositiveButton("Add", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                adapter.addLocation(input.getText().toString());
                adapter.notifyDataSetChanged();
            }
        });
        builder.setNegativeButton("Cancel", null);
        builder.create();
        builder.show();
    }
}
