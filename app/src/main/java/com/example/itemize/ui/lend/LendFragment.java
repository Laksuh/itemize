package com.example.itemize.ui.lend;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.itemize.Item;
import com.example.itemize.ItemAdapter;
import com.example.itemize.R;
import com.example.itemize.SaveLoad;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

public class LendFragment extends Fragment {

    private ArrayList<Lending> lendings = new ArrayList<>();
    private ArrayList<Item> items = new ArrayList<>();
    private LendingAdapter adapter;
    private SaveLoad saveLoad;

    @RequiresApi(api = Build.VERSION_CODES.O)
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        LendViewModel lendViewModel = ViewModelProviders.of(this).get(LendViewModel.class);
        View root = inflater.inflate(R.layout.fragment_lend, container, false);
        final TextView textView = root.findViewById(R.id.text_lendings);
        saveLoad = new SaveLoad(this.getContext());
        //init RecyclerView
        final RecyclerView recyclerView;
        recyclerView = root.findViewById(R.id.rvLendings);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this.getActivity());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        items = loadItems();
        lendings = Lending.createLendings(getContext());
         /*for (Lending lending: lendings){
            try {
                lendingExpired(lending);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }*/
        final LendingAdapter adapter = new LendingAdapter(lendings, this.getContext());
        this.adapter=adapter;
        recyclerView.setAdapter(adapter);

        FloatingActionButton addLending = root.findViewById(R.id.fabAddLending);

        adapter.setOnItemClickListener(new ItemAdapter.ClickListener() {
            @Override
            public void onItemLongClick(final int position, View v) {
                AlertDialog.Builder builder2 = new AlertDialog.Builder(v.getContext());
                builder2.setTitle("Lending finished?");
                builder2.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        adapter.removeLending(position);
                        adapter.notifyDataSetChanged();
                    }
                });
                builder2.setNeutralButton("No", null);
                builder2.setNegativeButton("Delete", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        adapter.removeLending(position);
                        recyclerView.removeViewAt(position);
                        adapter.notifyItemRemoved(position);
                        adapter.notifyItemRangeChanged(position, adapter.getItemCount());
                        adapter.notifyDataSetChanged();
                    }
                });
                builder2.create();
                builder2.show();
            }
        });

        addLending.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addLending();
            }
        });

        return root;
    }

    public void addLending(){
        final Dialog dialog = new Dialog(Objects.requireNonNull(getActivity()));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.add_lending);

        final EditText inputP2 = dialog.findViewById(R.id.editText2);
        final Spinner spinner = dialog.findViewById(R.id.spinner);
        final EditText etd = dialog.findViewById(R.id.editTextDate);

        List<String> spintems = new ArrayList<>();
        for(Item item : items){
            spintems.add(item.getName());
        }

        ArrayAdapter<String> sadapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, spintems);
        sadapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        spinner.setAdapter(sadapter);
        Button btnDP = dialog.findViewById(R.id.btnDP);
        btnDP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogFragment datePicker = new DatePickerFragment(etd);
                datePicker.show(Objects.requireNonNull(getFragmentManager()), "DatePicker");
            }
        });

        Button btnAdd = (Button) dialog.findViewById(R.id.btnAdd);
        Button btnCancel = (Button) dialog.findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etd.getText().toString().equals("until date")){
                    Toast toast = Toast.makeText(getContext(), "Select a date please!", Toast.LENGTH_SHORT);
                    toast.show();
                } else {
                    @SuppressLint("SimpleDateFormat") SimpleDateFormat DateFor = new SimpleDateFormat("dd/MM/yyyy");
                    items = loadItems();
                    Item targetItem = new Item("Error");
                    for (Item item : items) {
                        if (spinner.getSelectedItem().toString().equals(item.getName())) {
                            targetItem = item;
                            targetItem.setLend(true);
                            break;
                        }
                    }
                    saveItems(items);
                    Lending newLending = new Lending(targetItem, inputP2.getText().toString(), etd.getText().toString());
                    adapter.addLending(newLending);
                    adapter.notifyDataSetChanged();
                    /*try {
                        lendingExpired(newLending);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }*/
                    dialog.dismiss();
                }
            }
        });
        dialog.show();
    }

    private ArrayList<Item> loadItems(){
        return saveLoad.loadItems();
    }

    private void saveItems(ArrayList<Item> items){
        saveLoad.saveItems(items, "items.txt");
    }

    /*public void lendingExpired(Lending lending) throws ParseException {
        @SuppressLint("SimpleDateFormat") SimpleDateFormat dateFor = new SimpleDateFormat("dd/MM/yyyy");
        Date date = new Date();
        Date itemDate = dateFor.parse(lending.getDate());
        if(date.after(itemDate)){
            for(Item item : items){
                if(item.getName().equals(lending.getLendItem().getName())){
                    item.setLend(false);
                    break;
                }
            }
        }
        saveItems(items);
    }*/

}