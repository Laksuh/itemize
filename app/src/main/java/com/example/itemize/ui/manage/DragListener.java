package com.example.itemize.ui.manage;

import androidx.recyclerview.widget.RecyclerView;
import android.view.DragEvent;
import android.view.View;

import com.example.itemize.Item;
import com.example.itemize.R;
import java.util.ArrayList;

public class DragListener implements View.OnDragListener {

    private boolean isDropped = false;
    private Listener listener;
    private ManageItemAdapter mia;

    /*DragListener(Listener listener) {
        this.listener = listener;
    }*/

    DragListener(Listener listener, ManageItemAdapter mia) {
        this.listener = listener;
        this.mia = mia;
    }


    @Override
    public boolean onDrag(View v, DragEvent event) {
        if (event.getAction() == DragEvent.ACTION_DROP) {
            isDropped = true;
            int positionTarget = -1;

            View viewSource = (View) event.getLocalState();
            int viewId = v.getId();
            final int flItem = R.id.frame_layout_item;
            final int rvTop = R.id.rvTop;
            final int rvBottom = R.id.rvBottom;
            final int tvEmptyListTop = R.id.tvEmptyListTop;
            final int tvEmptyListBottom = R.id.tvEmptyListBottom;

            switch (viewId) {
                case flItem:
                case tvEmptyListTop:
                case tvEmptyListBottom:
                case rvTop:
                case rvBottom:

                    RecyclerView target;
                    switch (viewId) {
                        case tvEmptyListTop:
                        case rvTop:
                            target = v.getRootView().findViewById(rvTop);
                            break;
                        case tvEmptyListBottom:
                        case rvBottom:
                            target = v.getRootView().findViewById(rvBottom);
                            break;
                        default:
                            target = (RecyclerView) v.getParent();
                            positionTarget = (int) v.getTag();
                    }

                    if (viewSource != null) {
                        RecyclerView source = (RecyclerView) viewSource.getParent();

                        ManageItemAdapter adapterSource = (ManageItemAdapter) source.getAdapter();
                        int positionSource = (int) viewSource.getTag();
                        int sourceId = source.getId();

                        assert adapterSource != null;
                        Item item = adapterSource.getList().get(positionSource);
                        ArrayList<Item> listSource = adapterSource.getList();

                        listSource.remove(positionSource);
                        adapterSource.updateList(listSource);
                        adapterSource.notifyDataSetChanged();

                        ManageItemAdapter adapterTarget = (ManageItemAdapter) target.getAdapter();
                        assert adapterTarget != null;
                        ArrayList<Item> customListTarget = adapterTarget.getList();
                        if (positionTarget >= 0) {
                            customListTarget.add(positionTarget, item);
                            System.out.println(customListTarget);
                        } else {
                            customListTarget.add(item);
                        }
                        if (sourceId == rvBottom) {
                            mia.changeItemsLocaiton(item, 0);
                            System.out.println("DONE TOP");
                        }
                        if (sourceId == rvTop) {
                            mia.changeItemsLocaiton(item, 1);
                            System.out.println("DONE BOTTOM");
                        }
                        adapterTarget.updateList(customListTarget);
                        adapterTarget.notifyDataSetChanged();

                        if (sourceId == rvBottom && adapterSource.getItemCount() < 1) {
                            listener.setEmptyListBottom(true);
                        }
                        if (viewId == tvEmptyListBottom) {
                            listener.setEmptyListBottom(false);
                        }
                        if (sourceId == rvTop && adapterSource.getItemCount() < 1) {
                            listener.setEmptyListTop(true);
                        }
                        if (viewId == tvEmptyListTop) {
                            listener.setEmptyListTop(false);
                        }
                    }
                    break;
            }
        }

        if (!isDropped && event.getLocalState() != null) {
            ((View) event.getLocalState()).setVisibility(View.VISIBLE);
        }

        return true;
    }


}