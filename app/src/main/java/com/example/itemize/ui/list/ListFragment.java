package com.example.itemize.ui.list;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.itemize.Item;
import com.example.itemize.ItemAdapter;
import com.example.itemize.R;
import com.example.itemize.SaveLoad;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;

public class ListFragment extends Fragment {

    private ListViewModel listViewModel;
    private ArrayList<Item> items;
    private ItemAdapter adapter;
    private SaveLoad saveLoad;


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        listViewModel =
                ViewModelProviders.of(this).get(ListViewModel.class);
        View root = inflater.inflate(R.layout.fragment_list, container, false);
        final TextView textView = root.findViewById(R.id.text_home);
        listViewModel.getText().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });
        //end init fragment

        //ManageLocationsActivity
        Button btnManagelocations = root.findViewById(R.id.btnManageLocations);
        btnManagelocations.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), ManageLocationActivity.class);
                startActivity(intent);
            }
        });
        //additional code goes here

        //Init Recyclerview with Gridlayout for the Items + create Items
        final RecyclerView recyclerView;
        recyclerView = root.findViewById(R.id.rvItems);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(this.getActivity(), 4);
        recyclerView.setLayoutManager(layoutManager);

        recyclerView.setHasFixedSize(true);

        items = Item.createItemList(getContext());

        final ItemAdapter adapter = new ItemAdapter(items, this.getContext());
        this.adapter=adapter;
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(layoutManager);
        //end init RecyclerView

        //init button
        FloatingActionButton addItem = root.findViewById(R.id.fabAddItem);

        addItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addItemDialog(view);
            }
        });
        //end init button

        //init LongItemClickListener
        adapter.setOnItemClickListener(new ItemAdapter.ClickListener() {
            @Override
            public void onItemLongClick(final int position, View v) {
                AlertDialog.Builder builder2 = new AlertDialog.Builder(v.getContext());
                final EditText input2 = new EditText(v.getContext());
                input2.setText(ItemAdapter.getmItems().get(position).getName());
                builder2.setView(input2);
                builder2.setTitle("Edit " + ItemAdapter.getmItems().get(position).getName());
                builder2.setMessage("Rename item:");
                builder2.setPositiveButton("Rename", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        adapter.renameItem(position,input2.getText().toString());
                        adapter.notifyDataSetChanged();
                    }
                });
                builder2.setNeutralButton("Cancel", null);
                builder2.setNegativeButton("Delete", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        adapter.removeItem(position);
                        recyclerView.removeViewAt(position);
                        adapter.notifyItemRemoved(position);
                        adapter.notifyItemRangeChanged(position, adapter.getItemCount());
                        adapter.notifyDataSetChanged();
                    }
                });
                builder2.create();
                builder2.show();
            }
        });
        //end init LongItemClickListener

        return root;
    }

    private void addItemDialog(View view){
        AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
        final EditText input = new EditText(view.getContext());
        builder.setView(input);
        builder.setTitle("Add item");
        builder.setMessage("Item name:");
        builder.setPositiveButton("Add", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                adapter.addItem(input.getText().toString());
                adapter.notifyDataSetChanged();
            }
        });
        builder.setNegativeButton("Cancel", null);
        builder.create();
        builder.show();
    }
}