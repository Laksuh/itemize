package com.example.itemize.ui.lend;

import android.content.Context;
import android.os.Build;

import androidx.annotation.RequiresApi;

import com.example.itemize.Item;
import com.example.itemize.SaveLoad;

import java.util.ArrayList;

public class Lending {

    private Item lendItem;
    private String lendingPerson;
    private String date;

    public Lending(Item lendItem, String lendingPerson, String date) {
        this.lendingPerson = lendingPerson;
        this.date = date;
        this.lendItem = lendItem;
    }
    public Lending(Item lendItem) {
        this.lendItem = lendItem;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public static ArrayList<Lending> createLendings(Context context){
        SaveLoad saveLoad = new SaveLoad(context);
        try {
            return saveLoad.loadLendings();
        } catch(RuntimeException e) {
            ArrayList<Lending> lendings = new ArrayList<>();
            lendings.add(new Lending(new Item("Error while"), "loading lendings", "try again..."));
            return lendings;
        }
    }

    //Getters and Setters
    public String getLendingPerson() {
        return lendingPerson;
    }

    public void setLendingPerson(String lendingPerson) {
        this.lendingPerson = lendingPerson;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Item getLendItem() {
        return lendItem;
    }

    public void setLendItem(Item lendItem) {
        this.lendItem = lendItem;
    }
}
