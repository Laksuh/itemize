package com.example.itemize.ui.manage;

import android.annotation.SuppressLint;
import android.content.ClipData;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.example.itemize.Item;
import com.example.itemize.Location;
import com.example.itemize.R;
import com.example.itemize.SaveLoad;

import java.util.ArrayList;

public class ManageItemAdapter extends RecyclerView.Adapter<ManageItemAdapter.ViewHolder> implements View.OnTouchListener{

    private ArrayList<Item> items;
    private Listener listener;
    private SaveLoad saveLoad;
    private Context context;
    private ArrayList<Location> mLocations;
    private ManageFragment mf;

    ManageItemAdapter(ArrayList<Item> items, Listener listener) {
        this.items = items;
        this.listener = listener;
    }

    ManageItemAdapter(ArrayList<Item> items, Listener listener, Context context, ManageFragment mf) {
        this.items = items;
        this.listener = listener;
        this.context = context;
        this.saveLoad = new SaveLoad(this.context);
        this.mf = mf;
        mLocations = loadLocations();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(
                parent.getContext()).inflate(R.layout.item, parent, false);
        return new ViewHolder(view);
    }

    @SuppressLint({"SetTextI18n", "ClickableViewAccessibility"})
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.text.setText(items.get(position).getName());
        if (items.get(position).isLend()){
            holder.text.setTextColor(Color.RED);
            holder.cv.setCardBackgroundColor(Color.LTGRAY);
        }
        if(!items.get(position).isLend()){
            holder.text.setTextColor(Color.BLACK);
            holder.cv.setCardBackgroundColor(Color.WHITE);
        }
        holder.cl.setTag(position);
        holder.cl.setOnTouchListener(this);
        holder.cl.setOnDragListener(new DragListener(listener, this));
    }


    @Override
    public int getItemCount() {
        return items.size();
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            ClipData data = ClipData.newPlainText("", "");
            View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(v);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                v.startDragAndDrop(data, shadowBuilder, v, 0);
            } else {
                v.startDrag(data, shadowBuilder, v, 0);
            }
            return true;
        }
        return false;
    }

    ArrayList<Item> getList() {
        return items;
    }

    void updateList(ArrayList<Item> items) {
        this.items = items;
        //saveItems();
    }

    public void changeItemsLocaiton(Item item, int i){
        item.setLocation(mLocations.get(i));
        mf.saveItems();
    }

    DragListener getDragInstance() {
        if (listener != null) {
            System.out.println("getDragInstance");
            return new DragListener(listener, this);
        } else {
            System.out.println("ListAdapter"+ " Listener wasn't initialized!");
            return null;
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView text;
        ConstraintLayout cl;
        CardView cv;

        ViewHolder(View itemView) {
            super(itemView);
            text = itemView.findViewById(R.id.itemNameTextView);
            cl = itemView.findViewById(R.id.frame_layout_item);
            cv = itemView.findViewById(R.id.cv);
            mLocations = loadLocations();
        }
    }

    /*public void saveItems(){
        saveLoad.saveItems(items, "items.txt");
    }*/

    public ArrayList<Location> loadLocations(){
        return saveLoad.loadLocations();
    }

}
