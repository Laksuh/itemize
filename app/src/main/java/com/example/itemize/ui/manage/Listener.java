package com.example.itemize.ui.manage;

public interface Listener {

        void setEmptyListTop(boolean visibility);

        void setEmptyListBottom(boolean visibility);

}
