package com.example.itemize.ui.lend;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.itemize.Item;
import com.example.itemize.ItemAdapter;
import com.example.itemize.R;
import com.example.itemize.SaveLoad;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class LendingAdapter extends RecyclerView.Adapter<LendingAdapter.mViewHolder> {

    private static ItemAdapter.ClickListener clickListener;
    private ArrayList<Item> items;
    public Context context;
    private SaveLoad saveLoad;
    private static List<Lending> mLendings;

    public class mViewHolder extends RecyclerView.ViewHolder implements View.OnLongClickListener {
        TextView ItemNameTextView;
        TextView LendingPersonTextView;
        TextView DateTextView;

        public mViewHolder(View itemView){
            super(itemView);
            itemView.setOnLongClickListener(this);
            ItemNameTextView = itemView.findViewById(R.id.tvLendingItem);
            LendingPersonTextView = itemView.findViewById(R.id.tvLendingPerson);
            DateTextView = itemView.findViewById(R.id.tvLendingDate);
            items = loadItems();
        }

        @Override
        public boolean onLongClick(View v) {
            System.out.println(this.ItemNameTextView.getText());
            clickListener.onItemLongClick(getAdapterPosition(), v);
            return false;
        }
    }

    //LongClickListener set-up
    public void setOnItemClickListener(ItemAdapter.ClickListener clickListener) {
        LendingAdapter.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemLongClick(int position, View v);
    }

    public LendingAdapter(List<Lending> lendings, Context context) {
        mLendings= lendings;
        this.context = context;
        this.saveLoad = new SaveLoad(this.context);
        items = loadItems();
    }

    //set ViewHolder
    @NonNull
    @Override
    public LendingAdapter.mViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View itemView = inflater.inflate(R.layout.lending, parent, false);
        return new LendingAdapter.mViewHolder(itemView);
    }

    public void addLending(Lending lending){
        mLendings.add(lending);
        saveLendings();
    }

    public void removeLending(Lending lending){
        mLendings.remove(lending);
        saveLendings();
    }

    public void removeLending(int pos){
        Item lendItem = mLendings.get(pos).getLendItem();
        for(Item item : items){
            if(item.getName().equals(lendItem.getName())){
                item.setLend(false);
                break;
            }
        }
        saveLoad.saveItems(items, "items.txt");
        mLendings.remove(pos);
        saveLendings();
    }

    //Change or set individual values
    @Override
    public void onBindViewHolder(@NonNull LendingAdapter.mViewHolder holder, int position) {
        try {
            holder.ItemNameTextView.setText(mLendings.get(position).getLendItem().getName());
            holder.LendingPersonTextView.setText(mLendings.get(position).getLendingPerson());
            holder.DateTextView.setText(mLendings.get(position).getDate());
            Date now = new Date();
            @SuppressLint("SimpleDateFormat") SimpleDateFormat dateFor = new SimpleDateFormat("dd/MM/yyyy");
            Date date = dateFor.parse(mLendings.get(position).getDate());
            if (now.after(date)){
                holder.DateTextView.setTextColor(Color.RED);
            }
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    public static List<Lending> getmLendings() {
        return mLendings;
    }

    @Override
    public int getItemCount() {
        return mLendings.size();
    }

    private void saveLendings(){
        saveLoad.saveLendings(mLendings, "lendings.txt");
    }

    public ArrayList<Item> loadItems(){
        return saveLoad.loadItems();
    }
}
