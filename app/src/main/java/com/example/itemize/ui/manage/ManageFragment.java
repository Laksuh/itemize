package com.example.itemize.ui.manage;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.itemize.Item;
import com.example.itemize.Location;
import com.example.itemize.R;
import com.example.itemize.SaveLoad;
import com.example.itemize.ui.lend.LendFragment;
import com.example.itemize.ui.lend.Lending;

import java.util.ArrayList;

public class ManageFragment extends Fragment implements Listener{

    private ManageViewModel manageViewModel;
    private RecyclerView rvTop;
    private RecyclerView rvBottom;
    private TextView tvEmptyListBottom;
    private TextView tvEmptyListTop;
    private TextView tvrvTop;
    private TextView tvrvBottom;
    private ManageItemAdapter bottomListAdapter;
    private ManageItemAdapter topListAdapter;
    private SaveLoad saveLoad;
    private ArrayList<Location> locations;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        manageViewModel =
                ViewModelProviders.of(this).get(ManageViewModel.class);
        View root = inflater.inflate(R.layout.fragment_manage, container, false);

        rvTop = root.findViewById(R.id.rvTop);
        rvBottom = root.findViewById(R.id.rvBottom);
        tvrvTop = root.findViewById(R.id.tvrvTop);
        tvrvBottom = root.findViewById(R.id.tvrvBottom);
        tvEmptyListBottom = root.findViewById(R.id.tvEmptyListBottom);
        tvEmptyListTop = root.findViewById(R.id.tvEmptyListTop);
        saveLoad = new SaveLoad(this.getContext());
        initTopRecyclerView();
        initBottomRecyclerView();
        LendFragment lf = new LendFragment();
        return root;
    }

    private void initTopRecyclerView() {
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(this.getActivity(), 4);
        rvTop.setLayoutManager(layoutManager);

        ArrayList<Item> topList = Item.createItemList(getContext());
        ArrayList<Item> topListRemove = new ArrayList<>();

        locations = Location.createLocationList(this.getContext());
        if(locations.size() <=1){
            locations.add(new Location("change name of this location", 0));
            locations.add(new Location("change name of this location as well", 1));
            saveLoad.saveLocations(locations, "locations.txt");
        }
        tvrvTop.setText(locations.get(0).getName());
        for(Item item: topList){
            try {
                if (item.getLocation().getItemCount() == locations.get(1).getItemCount()) {
                    topListRemove.add(item);
                }
            } catch (NullPointerException e){
                e.printStackTrace();
            }
        }
        topList.removeAll(topListRemove);
        if(topList.isEmpty()){
            tvEmptyListTop.setVisibility(View.VISIBLE);
        }
        topListAdapter = new ManageItemAdapter(topList, this, getContext(), this);
        rvTop.setAdapter(topListAdapter);
        tvEmptyListTop.setOnDragListener(topListAdapter.getDragInstance());
        rvTop.setOnDragListener(topListAdapter.getDragInstance());
    }

    private void initBottomRecyclerView() {
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(this.getActivity(), 4);
        rvBottom.setLayoutManager(layoutManager);

        ArrayList<Item> bottomList = Item.createItemList(this.getContext());
        ArrayList<Item> bottomListRemove = new ArrayList<>();

        tvrvBottom.setText(locations.get(1).getName());

        for(Item item: bottomList){
            try {
                if (item.getLocation().getItemCount() == locations.get(0).getItemCount()) {
                    bottomListRemove.add(item);
                }
            } catch (NullPointerException e){
                e.printStackTrace();
            }
        }
        bottomList.removeAll(bottomListRemove);
        if(bottomList.isEmpty()){
            tvEmptyListBottom.setVisibility(View.VISIBLE);
        }
        bottomListAdapter = new ManageItemAdapter(bottomList, this, this.getContext(), this);
        rvBottom.setAdapter(bottomListAdapter);
        tvEmptyListBottom.setOnDragListener(bottomListAdapter.getDragInstance());
        rvBottom.setOnDragListener(bottomListAdapter.getDragInstance());
    }

    @Override
    public void setEmptyListTop(boolean visibility) {
        tvEmptyListTop.setVisibility(visibility ? View.VISIBLE : View.GONE);
        rvTop.setVisibility(visibility ? View.GONE : View.VISIBLE);
    }

    @Override
    public void setEmptyListBottom(boolean visibility) {
        tvEmptyListBottom.setVisibility(visibility ? View.VISIBLE : View.GONE);
        rvBottom.setVisibility(visibility ? View.GONE : View.VISIBLE);
    }

    public void saveItems(){
        ArrayList<Item> items = new ArrayList<>();
        items.addAll(bottomListAdapter.getList());
        items.addAll(topListAdapter.getList());
        saveLoad.saveItems(items, "items.txt");
    }
}