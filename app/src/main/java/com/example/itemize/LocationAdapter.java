package com.example.itemize;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class LocationAdapter extends RecyclerView.Adapter<LocationAdapter.mViewHolder> {

    //Attributes
    private static ItemAdapter.ClickListener clickListener;
    private static List<Location> mLocations;
    public Context context;
    private SaveLoad saveLoad;

    //Bind view to individual items
    public class mViewHolder extends RecyclerView.ViewHolder implements View.OnLongClickListener {
        public ImageView locationImageView;
        public TextView locationNameTextView;

        public mViewHolder(View itemView){
            super(itemView);
            itemView.setOnLongClickListener(this);
            locationImageView = itemView.findViewById(R.id.itemImageView);
            locationNameTextView = itemView.findViewById(R.id.itemNameTextView);
        }

        @Override
        public boolean onLongClick(View v) {
            System.out.println(this.locationNameTextView.getText());
            clickListener.onItemLongClick(getAdapterPosition(), v);
            return false;
        }
    }

    //LongClickListener set-up
    public void setOnItemClickListener(ItemAdapter.ClickListener clickListener) {
        LocationAdapter.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemLongClick(int position, View v);
    }

    //renameItem
    public void renameLocation(int position, String newName){
        mLocations.get(position).setName(newName);
        saveLocations();
    }

    //removeItem
    public void removeLocation(int position){
        mLocations.remove(position);
        saveLocations();
    }

    //Constructor
    public LocationAdapter(List<Location> locations, Context context) {
        mLocations= locations;
        this.context = context;
        this.saveLoad = new SaveLoad(this.context);
    }

    //Method to add items
    public void addLocation(String name){
        mLocations.add(new Location(name));
        saveLocations();
    }

    //set ViewHolder
    @NonNull
    @Override
    public LocationAdapter.mViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View itemView = inflater.inflate(R.layout.location, parent, false);
        return new mViewHolder(itemView);
    }

    //Change or set individual values
    @Override
    public void onBindViewHolder(@NonNull LocationAdapter.mViewHolder holder, int position) {
        try {
            holder.locationNameTextView.setText(mLocations.get(position).getName());
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    //Standard methods
    public static List<Location> getmLocations() {
        return mLocations;
    }

    @Override
    public int getItemCount() {
        return mLocations.size();
    }

    public void saveLocations(){
        saveLoad.saveLocations(mLocations, "locations.txt");
    }

    public ArrayList<Location> loadLocations(){
        return saveLoad.loadLocations();
    }

}
