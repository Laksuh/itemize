package com.example.itemize;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class ItemAdapter extends RecyclerView.Adapter<ItemAdapter.mViewHolder> {

    //Attributes
    private static ItemAdapter.ClickListener clickListener;
    private static List<Item> mItems;
    public Context context;
    private SaveLoad saveLoad;
    private ArrayList<Location> mLocations;//= loadLocations();

    //Bind view to individual items
    public class mViewHolder extends RecyclerView.ViewHolder implements View.OnLongClickListener {
        ImageView itemImageView;
        TextView ItemNameTextView;

        public mViewHolder(View itemView){
            super(itemView);
            itemView.setOnLongClickListener(this);
            itemImageView = itemView.findViewById(R.id.itemImageView);
            ItemNameTextView = itemView.findViewById(R.id.itemNameTextView);
            mLocations = loadLocations();
        }

        @Override
        public boolean onLongClick(View v) {
            System.out.println(this.ItemNameTextView.getText());
            clickListener.onItemLongClick(getAdapterPosition(), v);
            return false;
        }
    }

    //LongClickListener set-up
    public void setOnItemClickListener(ItemAdapter.ClickListener clickListener) {
        ItemAdapter.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemLongClick(int position, View v);
    }

    //renameItem
    public void renameItem(int position, String newName){
        mItems.get(position).setName(newName);
        saveItems();
    }

    //removeItem
    public void removeItem(int position){
        mItems.remove(position);
        saveItems();
    }

    //Constructor
    public ItemAdapter(List<Item> items, Context context) {
        mItems= items;
        this.context = context;
        this.saveLoad = new SaveLoad(this.context);
        mLocations = loadLocations();
    }

    //Method to add items
    public void addItem(String name){
        mLocations = loadLocations();
        Item item = new Item(name, mLocations.get(0));
        System.out.println(item.getLocation().getName());
        mItems.add(item);
        saveItems();
    }

    public void addItem(Item item){
        mLocations = loadLocations();
        mItems.add(item);
        saveItems();
    }

    //leave this method as it is please :)
    //set ViewHolder
    @NonNull
    @Override
    public ItemAdapter.mViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View itemView = inflater.inflate(R.layout.item, parent, false);
        return new mViewHolder(itemView);
    }

    //Change or set individual values
    @Override
    public void onBindViewHolder(@NonNull mViewHolder holder, int position) {
        try {
            //holder.itemImageView.setImageResource(R.drawable.ic_home_black_24dp); ITEMS BESITZEN NOCH KEINE BILDER
            holder.ItemNameTextView.setText(mItems.get(position).getName());
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    //Standard methods
    public static List<Item> getmItems() {
        return mItems;
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    private void saveItems(){
        saveLoad.saveItems(mItems, "items.txt");
    }

    public ArrayList<Item> loadItems(){
        return saveLoad.loadItems();
    }

    public ArrayList<Location> loadLocations(){
        return saveLoad.loadLocations();
    }

}
