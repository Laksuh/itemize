package com.example.itemize;

import android.content.Context;
import android.os.Build;

import androidx.annotation.RequiresApi;

import com.example.itemize.ui.lend.Lending;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

public class SaveLoad {

    private Context context;

    public SaveLoad(Context context){
        this.context=context;
    }

    public void saveItems(List<Item> items, String fileName){
        File path =  context.getFilesDir();
        File paramsFile = new File(path, fileName);

        try (Writer fWriter = new FileWriter(paramsFile)){
            for (int i=0; i<items.size(); i++){
                fWriter.write(items.get(i).getName()+","); //saves items name
                fWriter.write(items.get(i).getDescription()+","); //saves description
                fWriter.write(items.get(i).getLocation().getItemCount()+","); //saves location
               // fWriter.write(items.get(i).getItemImageView()+","); //saves image
                fWriter.write(items.get(i).isLend()+"\n"); //saves if it is lend
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void saveLocations(List<Location> items, String fileName){
        File path =  context.getFilesDir();
        File paramsFile = new File(path, fileName);

        try (Writer fWriter = new FileWriter(paramsFile)){
            for (int i=0; i<items.size(); i++){
                fWriter.write(items.get(i).getName()+",");
                fWriter.write(items.get(i).getItemCount()+"\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void saveLendings(List<Lending> lendings, String fileName){
        File path =  context.getFilesDir();
        File paramsFile = new File(path, fileName);

        try (Writer fWriter = new FileWriter(paramsFile)){
            for (int i=0; i<lendings.size(); i++){
                fWriter.write(lendings.get(i).getLendItem().getName()+",");
                fWriter.write(lendings.get(i).getLendingPerson()+",");
                fWriter.write(lendings.get(i).getDate()+"\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //Load items to init them
    public ArrayList<Item> loadItems(){
        ArrayList<Item> mItems = new ArrayList<>();

        ArrayList<String> names = new ArrayList<>();
        ArrayList<String> descriptions = new ArrayList<>();
        ArrayList<String> locations = new ArrayList<>();
        ArrayList<String> lend = new ArrayList<>();
        // ArrayList<String> images = new ArrayList<>();
        try {
            File path = this.context.getFilesDir();
            String fileName = "items.txt";
            File paramsFile = new File(path, fileName);
            try (BufferedReader br = new BufferedReader(new FileReader(paramsFile))) {
                while (br.ready()) {
                    String line = br.readLine();
                    String[] actualLine = line.split(",");
                    names.add(actualLine[0]);
                    descriptions.add(actualLine[1]);
                    locations.add(actualLine[2]);
                    lend.add(actualLine[3]);
                   // images.add(actualLine[3]);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }catch (NumberFormatException e) {
            e.printStackTrace();
        }

        //Make 1 ArrayList with items instead of returning many
        for (int i=0;i<names.size();i++){
            mItems.add(new Item(names.get(i)));
            mItems.get(i).setDescription(descriptions.get(i));
            mItems.get(i).setLocation(new Location("irrelevant", Integer.parseInt(locations.get(i)))); //location von geladenen locations nehmen
            mItems.get(i).setLend(Boolean.parseBoolean(lend.get(i)));
            // mItems.get(i).getItemImageView().setImageResource(Integer.parseInt(images.get(i)));
        }

        return mItems;
    }

    //Load Locations to init them
    public ArrayList<Location> loadLocations(){
        ArrayList<Location> mLocations = new ArrayList<>();

        ArrayList<String> names = new ArrayList<>();
        ArrayList<String> counts = new ArrayList<>();
        try {
            File path = this.context.getFilesDir();
            String fileName = "locations.txt";
            File paramsFile = new File(path, fileName);
            try (BufferedReader br = new BufferedReader(new FileReader(paramsFile))) {

                while (br.ready()) {
                    String line = br.readLine();
                    String[] actualLine = line.split(",");
                    names.add(actualLine[0]);
                    counts.add(actualLine[1]);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }catch (NumberFormatException e) {
            e.printStackTrace();
        }

        for (int i=0;i<names.size();i++){
            mLocations.add(new Location(names.get(i), Integer.parseInt(counts.get(i))));
        }

        return mLocations;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public ArrayList<Lending> loadLendings(){
        ArrayList<Lending> mLendings = new ArrayList<>();

        ArrayList<String> items = new ArrayList<>();
        ArrayList<String> names = new ArrayList<>();
        ArrayList<String> dates = new ArrayList<>();
        // ArrayList<String> images = new ArrayList<>();
        //  ArrayList<String> lend = new ArrayList<>();
        try {
            File path = this.context.getFilesDir();
            String fileName = "lendings.txt";
            File paramsFile = new File(path, fileName);
            try (BufferedReader br = new BufferedReader(new FileReader(paramsFile))) {
                while (br.ready()) {
                    String line = br.readLine();
                    String[] actualLine = line.split(",");
                    items.add(actualLine[0]);
                    names.add(actualLine[1]);
                    dates.add(actualLine[2]);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }catch (NumberFormatException e) {
            e.printStackTrace();
        }
        for (int i=0;i<names.size();i++){
            mLendings.add(new Lending(new Item(items.get(i))));
            mLendings.get(i).setLendingPerson(names.get(i));
            mLendings.get(i).setDate(dates.get(i));
        }

        return mLendings;
    }

}
