package com.example.itemize;

import android.content.Context;

import java.io.Serializable;
import java.util.ArrayList;

public class Location implements Serializable {

    //Attributes
    private String name;
    private int id;

    //Constructor
    public Location(String name) {
        this.name = name;
        this.id = 0;
    }

    public Location(String name, int id) {
        this.name = name;
        this.id = id;
    }

    //Getters and Setters
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getItemCount() {
        return id;
    }

    public void setItemCount(int itemCount) {
        this.id = itemCount;
    }

    //Methods
    public static ArrayList<Location> createLocationList(Context context){
        ArrayList<Location> locations = new ArrayList<>();
        SaveLoad saveLoad = new SaveLoad(context);
        try {
            return saveLoad.loadLocations();
        } catch(RuntimeException e) {
            locations.add(new Location("Something"));
            locations.add(new Location("went"));
            locations.add(new Location("terribly"));
            locations.add(new Location("wrong :("));
            return locations;
        }
        //dummy items

    }
}