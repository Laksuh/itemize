package com.example.itemize;

import android.content.Context;
import android.widget.ImageView;

import java.io.Serializable;
import java.util.ArrayList;

public class Item implements Serializable {

    //Attributes
    private String name;
    private String description="Itemdescription";
   // private ImageView itemImageView;
    private Location location;
    private boolean lend=false;

    //Constructors Overloading
    public Item(String name){
        this.name = name;
        this.location = new Location("NO_LOCATION");
    }

    public Item(String name, Location location){
        this.name = name;
        this.location = location;
    }

    public Item(String name, String location){
        this.name = name;
        this.location = new Location(location);
    }

    public Item(String name, String description, ImageView itemImageView, Location location, boolean lend) {
        this.name = name;
        this.description = description;
        //this.itemImageView = itemImageView;
        this.location = location;
        this.lend = lend;
    }

    public Item(String name, String description, Location location, boolean lend) {
        this.name = name;
        this.description = description;
        this.location = location;
        this.lend = lend;
    }

    //Methods
    public static ArrayList<Item> createItemList(Context context){
        ArrayList<Item> items = new ArrayList<>();
        SaveLoad saveLoad = new SaveLoad(context);
        try {
            return saveLoad.loadItems();
        } catch(RuntimeException e) {
            items.add(new Item("Something"));
            items.add(new Item("went"));
            items.add(new Item("terribly"));
            items.add(new Item("wrong :("));
            return items;
        }
        //dummy items

    }

    //Getters and Setters
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

   /* public ImageView getItemImageView() {
        return itemImageView;
    }

    public void setItemImageView(ImageView imageView) {
        this.itemImageView = imageView;
    }*/

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public boolean isLend() {
        return lend;
    }

    public void setLend(boolean lend) {
        this.lend = lend;
    }

}
