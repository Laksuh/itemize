## Use Case Specification: Change items location

### Table of Contens
1.  Use-Case Name
    1. Brief Description
2. Flow of Events
    1. Basic Flow
3. Special Requirement
    1. Mobile Functionality
4. Postcondition
    1. Update List of locations
5. Extension Points

#### 1. Change items location

##### 1.1 Brief Description

This use case allows the user to change the location of their items, by simply drag and dropping them across their screen.

#### 1.2 Mock-up

![Mockup picture][mockup]

[mockup]: https://gitlab.com/Laksuh/itemize/raw/master/UseCases/ChangeItemsLocation/ChangeItemLocation-mockup.png "Mock-up"

#### 2. Flow of Events

##### 2.1 Basic Flow

![Flow-chart][flowchart]

[flowchart]: https://gitlab.com/Laksuh/itemize/raw/master/UseCases/ChangeItemsLocation/ChangeItemsLocation.png "Flow-chart"

#### 3. Special Requirements

##### 3.1 Mobile Functionality

The app will be written for Android devices, therefore it needs to fit the size of different screens and adapt to them if needed.
A release for iOS is not planned currently.

##### 3.2 Narrative

[Narrative (.feature) can be found here](https://gitlab.com/Laksuh/itemize/blob/master/app/src/androidTest/assets/ChangeItemsLocaiton.feature)

#### 4. Postcondition

##### 4.1 Update List of locations

The changes that the user made need to be saved and displayed once they made those changes.

#### 5. Extension Points

n/a

#### 6. Function Poins

https://docs.google.com/spreadsheets/d/e/2PACX-1vS36aWaat0yP2nSqX6XFT1YgyfSgC3z0vblbp9WJFXIzc9t33STTA3kAVyvMeBDbyrskoBWGo6Yn8xB/pubhtml?gid=112681915&single=true