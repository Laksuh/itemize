# Itemize
## Use Case Specification: Manage Items

### Table of Contens
1.  Manage Items
    1. Brief Description
    2. Mock-Up
2. Flow of Events
    1. Basic Flow
3. Special Requirement
    1. First Special Requirement
4. Preconditions
    1. Precondition One
5. Postcondition
    1. Postcondition One
6. Extension Points
    1. Name of Extension Point

### Use-Case Specification: Manage Items CRUD
#### 1. Manage Items
##### 1.1 Brief Description

This use case allows the user to manage the items the want to manage. They can add a new item, list them, edit them or delete them.

##### 1.2 Mock-Up

![Mockup picture][mockup]

[mockup]: https://gitlab.com/Laksuh/itemize/raw/master/UseCases/ManageItems/Manage_Items_-_MockUp.png "MockUp"


#### 2. Flow of Events
##### 2.1 Basic Flow

![Mockup picture][flowchart]

[flowchart]: https://gitlab.com/Laksuh/itemize/raw/master/UseCases/ManageItems/Manage_Items.png "Flow-chart"


#### 3. Special Requirements

The app will be written for Android devices, therefore it needs to fit the size of different screens and adapt to them if needed.
A release for iOS is not planned currently.

#### 4. Preconditions

n/a

#### 5. Postcondition

n/a

#### 6. Extension Points

n/a

#### 7. Function Poins

https://docs.google.com/spreadsheets/d/e/2PACX-1vS36aWaat0yP2nSqX6XFT1YgyfSgC3z0vblbp9WJFXIzc9t33STTA3kAVyvMeBDbyrskoBWGo6Yn8xB/pubhtml?gid=112681915&single=true