# Itemize
## Use Case Specification: Manage Lending

### Table of Contens
1.  Manage Lending
    1. Brief Description
    2. Mock-Up
2. Flow of Events
    1. Basic Flow
3. Special Requirement
    1. First Special Requirement
4. Preconditions
    1. Precondition One
5. Postcondition
    1. Postcondition One
6. Extension Points
    1. Name of Extension Point

### Use-Case Specification: Manage Lending CRUD
#### 1. Manage Lending

##### 1.1 Brief Description

This use case allows the user to manage the lendings with other persons. They can add a new lending, list them, edit them or archive them when the lendingis done.

##### 1.2 Mock-Up

![Mockup picture][mockup]

[mockup]: https://gitlab.com/Laksuh/itemize/raw/master/UseCases/ManageLending/Manage_Lendings_-_MockUp.png "MockUp"

#### 2. Flow of Events
##### 2.1 Basic Flow

![Mockup picture][flowchart]

[flowchart]: https://gitlab.com/Laksuh/itemize/raw/master/UseCases/ManageLending/Manage_Lendings.png "Flow-chart"

#### 3. Special Requirements
##### 3.1 Mobile Functionality

The app will be written for Android devices, therefore it needs to fit the size of different screens and adapt to them if needed.
A release for iOS is not planned currently.

#### 4. Preconditions
##### 4.1 Precondition One

To list the lendings, the user needs to be logged in.

#### 5. Postcondition

#### 6. Extension Points

n/a

#### 7. Function Poins

https://docs.google.com/spreadsheets/d/e/2PACX-1vS36aWaat0yP2nSqX6XFT1YgyfSgC3z0vblbp9WJFXIzc9t33STTA3kAVyvMeBDbyrskoBWGo6Yn8xB/pubhtml?gid=112681915&single=true