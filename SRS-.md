# Itemize - Software Requirements Specification

##### Version 1.0

## Table of Contents
- [1.	Introduction](https://gitlab.com/Laksuh/itemize/blob/master/SRS-.md#1)	
    - [1.1	Purpose](https://gitlab.com/Laksuh/itemize/blob/master/SRS-.md#1.1)	
    - [1.2	Scope](https://gitlab.com/Laksuh/itemize/blob/master/SRS-.md#1.2)	
    - [1.3	Definitions, Acronyms, and Abbreviations](https://gitlab.com/Laksuh/itemize/blob/master/SRS-.md#1.3)	
    - [1.4	References](https://gitlab.com/Laksuh/itemize/blob/master/SRS-.md#1.4)	
    - [1.5	Overview](https://gitlab.com/Laksuh/itemize/blob/master/SRS-.md#1.5)	
- [2.	Overall Description](https://gitlab.com/Laksuh/itemize/blob/master/SRS-.md#2)	
- [3.	Specific Requirements](https://gitlab.com/Laksuh/itemize/blob/master/SRS-.md#3)
    - [3.1	Functionality](https://gitlab.com/Laksuh/itemize/blob/master/SRS-.md#3.1) 
    - [3.1.1	Change items location](https://gitlab.com/Laksuh/itemize/blob/master/SRS-.md#3.1.1) 
    - [3.1.2	Manage location](https://gitlab.com/Laksuh/itemize/blob/master/SRS-.md#3.1.2) 
    - [3.1.3	Lend Items To...](https://gitlab.com/Laksuh/itemize/blob/master/SRS-.md#3.1.3) 
    - [3.1.4	Manage Lending](https://gitlab.com/Laksuh/itemize/blob/master/SRS-.md#3.1.4) 
    - [3.1.5    Manage Items](https://gitlab.com/Laksuh/itemize/blob/master/SRS-.md#3.1.5)
    - [3.2	Usability](https://gitlab.com/Laksuh/itemize/blob/master/SRS-.md#3.2) 
    - [3.2.1	Usability Requirements](https://gitlab.com/Laksuh/itemize/blob/master/SRS-.md#3.2.1)
    - [3.3	Reliability](https://gitlab.com/Laksuh/itemize/blob/master/SRS-.md#3.3)
    - [3.4	Performance](https://gitlab.com/Laksuh/itemize/blob/master/SRS-.md#3.4)
    - [3.5	Supportability](https://gitlab.com/Laksuh/itemize/blob/master/SRS-.md#3.5)		
    - [3.6	Design Constraints](https://gitlab.com/Laksuh/itemize/blob/master/SRS-.md#3.6)	
    - [3.7	On-line User Documentation and Help System Requirements](https://gitlab.com/Laksuh/itemize/blob/master/SRS-.md#3.7)
    - [3.8	Purchased Components](https://gitlab.com/Laksuh/itemize/blob/master/SRS-.md#3.8)	
    - [3.9	Interfaces](https://gitlab.com/Laksuh/itemize/blob/master/SRS-.md#3.9)
    - [3.9.1	User Interfaces](https://gitlab.com/Laksuh/itemize/blob/master/SRS-.md#3.9.1)	    
    - [3.9.2	Hardware Interfaces](https://gitlab.com/Laksuh/itemize/blob/master/SRS-.md#3.9.2)
    - [3.9.3	Software Interfaces](https://gitlab.com/Laksuh/itemize/blob/master/SRS-.md#3.9.3)
    - [3.9.4	Communications Interfaces](https://gitlab.com/Laksuh/itemize/blob/master/SRS-.md#3.9.4)	
    - [3.10	Licensing Requirements](https://gitlab.com/Laksuh/itemize/blob/master/SRS-.md#3.10)	
    - [3.11	Legal, Copyright, and Other Notices](https://gitlab.com/Laksuh/itemize/blob/master/SRS-.md#3.11)
    - [3.12	Applicable Standards](https://gitlab.com/Laksuh/itemize/blob/master/SRS-.md#3.12)	

### <a name="1"></a>1. Introduction


#### <a name="1.1"></a>1.1 Purpose

The purpose of this document gives a general description of project: Itemize. It explains our vision and all features of the product.

#### <a name="1.2"></a>1.2 Scope

This document is designed for internal use only and will outline the development process of the project.

#### <a name="1.3"></a>1.3 Definitions, Acronyms, and Abbreviations

| **Term** | **Definition/Acronym/Abbreviations** |
| --- | --- |
| Android | Most used mobile Platform |
| API | Application Programming Interface |
| FAQ | Frequently Asked Questions |
| SRS | Software Requirements Specification |

#### <a name="1.4"></a>1.4 References

| **Title + link** | **Report Number** | **Date** | **Publishing organization** |
| --- | --- | --- | --- |
| [Blog](https://itemizeproject.wordpress.com) | - | 20/10/2019 | Wordpress |
| [GitLab](https://gitlab.com/Laksuh/itemize/tree/master) | - | 20/10/2019 | GitLab |
| [Android](https://www.android.com/intl/de_de/) | - | 20/10/2019 | Google |

#### <a name="1.5"></a>1.5 Overview

The next chapters provides information about our vision based on the use case diagram as well as more detailed software requirements.

### <a name="2"></a>2. Overall Description

The idea is to gain an overview about our properties and items while commuting between multiple locations.

To realize this as comfortable as possible we want to create a tool which can be accessed from a device with a significant role in our day’s life. Or in other words, we want to create an application for Android smartphones.

This app shall be able to help you managing your important items to gain a better overview. Therefore the plan is to add locations and items. By simply dragging and dropping them to their actual location your items shall be always up to date.

We are looking ahead to realize the following features:

Add Locations & Items
Abstract your common places and your most used items. In this way you are able to see your accumulation and where your mess is distributed.
Organize and lend things
If you are going to lend something to your buddy, you’ll never forget about it, when you use Itemizes scheduling tool. You are able to mark items as lended and you’ll be remembered, when your set limit is expired.
Keep control
While you added your items you can drag and drop these between your specified locations
So the app focuses on people who are coming around pretty much, like students, commuters or simply the ones who like keeping an overview of their owned items.

For a more background information, have a look at our [blog](https://itemizeproject.wordpress.com).

If we are going to implement a membership system, we depend on a database, where all items, users and locations can be stored.

#### <a name="2.1"></a>2.1 Use-Case Diagram

To gain a better overview, we added an Use-Case Diagram. For a better over view, we added three times the same Actor, instead of just having one, with too many associations.

![alt text][logo]

[logo]: https://gitlab.com/Laksuh/itemize/raw/master/UCD.png "UCD"

### <a name ="3"></a>3. Specific Requirements
 
#### <a name ="3.1"></a>3.1 Functionality 

##### <a name ="3.1.1"></a>3.1.1 Change items location

[The specification can be found here](https://gitlab.com/Laksuh/itemize/blob/master/UseCases/ChangeItemsLocation/ChangeItemsLocation.md)

##### <a name ="3.1.2"></a>3.1.2 Manage location

[The specification can be found here](https://gitlab.com/Laksuh/itemize/blob/master/UseCases/ManageLocation/ManageLocation.md)

##### <a name ="3.1.3"></a>3.1.3 Lend item to...

[The specification can be found here](https://gitlab.com/Laksuh/itemize/blob/master/UseCases/lend%20item%20to/lendItemTo.md)

##### <a name ="3.1.4"></a>3.1.4 Manage lendings

[The specification can be found here](https://gitlab.com/Laksuh/itemize/blob/master/UseCases/ManageLending/ManageLending.md)

##### <a name ="3.1.5"></a>3.1.5 Manage Items

[The specification can be found here](https://gitlab.com/Laksuh/itemize/blob/master/UseCases/ManageItems/ManageItems.md)

#### <a name ="3.2"></a>3.2 Usability

- a normal user does not need more than 2 minutes to understand the simple management system.
- to change an items location the user needs about 1-2 seconds (when having more items, he needs additional time to find the item)
- to lend an item, about 10s are needed
- to manage items/locations the user usually needs about 5 to 15s, depending on the fact, if he is using/searching an icon
- We may be depending on [Androids design guidelines](https://developer.android.com/design)

##### <a name ="3.2.1"></a>3.2.1 Usability Requirements

The complete usability requirements we are aware of can be found [here](https://material.io/design/usability/accessibility.html#hierarchy).

#### <a name ="3.3"></a>3.3 Reliability 

- The user should not spend more than 5 minutes a day at our application
- The user should be able to have access on his data, even without internet connection
- the MTBF should be about an infinite ammount. If the database crashed or is shutted down for ever, the user is still able to manage and does not have any limitation of the applications goal
    --> The account system is an optional feature, if it will be implemented, the users data will be synced, once our systems are online/repaired again
- The MTTR depends on our willingness to look after the issue (We are just two students, are not paid for this project and the app is not depending on our database system)

#### <a name ="3.4"></a>3.4 Performance

The response times for the different Use Cases are barely notable, all transactions are done within less than a second of triggering them.
As it is a very simple app without the need to calculate a lot of things, it uses little resources and shouldn't crash because of performance issues

#### <a name ="3.5"></a>3.5 Supportability

n/a

#### <a name ="3.6"></a>3.6 Design Constraints

All design contraints were already given by the requirements of developing an Android Application

#### <a name ="3.7"></a>3.7 On-line User Documentation and Help System Requirements

We have a User Guide that can be easily accessed by any given browser (https://itemizeproject.wordpress.com/user-guide/)

#### <a name ="3.8"></a>3.8 Purchased Components

n/a

#### <a name ="3.9"></a>3.9 Interfaces

n/a

#### <a name ="3.9.1"></a>3.9.1 User Interfaces

The user is supposed to see an application with a bar split in 4 parts, one which leads to a list of the items and an option to add items and manage locations, one which leads to a management view of the items and the locations.
The third view is supposed to show the lendings made, and an option to add a lending. The final view shall show informations regarding the application and links to important websites.

####  <a name ="3.9.2"></a>3.9.2 Hardware Interfaces

n/a

##### <a name ="3.9.3"></a>3.9.3 Software Interfaces

n/a

##### <a name ="3.9.4"></a>3.9.4 Communications Interfaces

n/a

#### <a name ="3.10"></a>3.10 Licensing Requirements

n/a

#### <a name ="3.11"></a>3.11 Legal, Copyright, and Other Notices

Itemize retains the rights on their logo

####  <a name ="3.12"></a>3.12 Applicable Standards

n/a
