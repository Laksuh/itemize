# Test plan

## 1.	Introduction
### 1.1	Purpose
The purpose of the Iteration Test Plan is to gather all of the information necessary to plan and control the test effort for a given iteration. 
It describes the approach to testing the software.
This Test Plan for **Itemize** supports the following objectives:
-	Identifies the items that should be targeted by the tests.
-	Identifies the motivation for and ideas behind the test areas to be covered.
-	Outlines the testing approach that will be used.
-	Identifies the required resources and provides an estimate of the test efforts.

### 1.2	Scope
This document describes the used tests, as they are unit tests and functionality testing.

### 1.3	Intended Audience
This document is meant for internal use primarily.

### 1.4	Document Terminology and Acronyms
- **SRS**	Software Requirements Specification
- **n/a**	not applicable
- **tbd**	to be determined

### 1.5	 References
| Reference        | 
| ------------- |
| [SAD](https://gitlab.com/Laksuh/itemize/-/blob/master/SAD.md) | 
| [Function Points](https://docs.google.com/spreadsheets/d/1RRrO3F_BD46rCI8w_hGv11EgPPz6ZL9e3dS2ue8jF8Y/edit#gid=112681915) |
| [UCs](https://gitlab.com/Laksuh/itemize/-/tree/master/UseCases) |

            
## 2.	Evaluation Mission and Test Motivation
### 2.1	Background
By testing source code, we ensure our application to run smoothly. The goal is to make sure, that our application does not run into any unexpected errors.
### 2.2	Evaluation Mission
The mission of this test plan is to prevent errors and ensure an outstanding software quality.
### 2.3	Test Motivators
Our testing is motivated by 
- quality risks 
- technical risks, 
- use cases 
- functional requirements

## 3.	Target Test Items
The listing below identifies those test items (software, hardware, and supporting product elements) that have been identified as targets for testing. 
This list represents what items will be tested. 

Items for Testing:
- Adapters and Methods (functionality of Itemize UCs (Java))
- UI testing (does the UI react correctly on User Input)

## 4.	Outline of Planned Tests
### 4.1	Outline of Test Inclusions
Unit Testing our Methods. (F.e. if an item is dragged on any position, is it handled or does it cause a crash?). <br/>
Functional Testing for the Frontend. <br/>

### 4.2	Outline of Other Candidates for Potential Inclusion
Stress Testing the application and its devices storage. Simulating overflows of data a.s.o.

### 4.3 Outline of Test Exclusions
Unit Testing in the frontend is currently not part of the planned strategy

## 5.	Test Approach
### 5.1 Initital Test-Idea Catalogs and Other Reference Sources
**n/a**
### 5.2	Testing Techniques and Types
#### 5.2.1 Adapters and Methods
|| |
|---|---|
|Technique Objective  	| Several functions in the needed Activities will be called, their results will be compared with predefined results |
|Technique 		| Testdata is generated and the methods of the Activities handle the queries |
|Oracles 		| Functions return the correct and expected data. |
|Required Tools 	| Android Studio (Emulator) |
|Success Criteria	| expected responses, passing tests |
|Special Considerations	|     -          |

#### 5.2.2 UI testing
|| |
|---|---|
|Technique Objective  	| Every interaction with the frontend should function as intended. |
|Technique 		| To test the technology user input is simulated and handled by the frontend.  |
|Oracles 		| information is shown as expected |
|Required Tools 	| Gherkin, Android Studio (Emulator) |
|Success Criteria	| Expected behavior and passing tests |
|Special Considerations	|     -          |

#### 5.2.3 Business Cycle Testing
**n/a**

#### 5.2.4 User Interface Testing
**n/a**

#### 5.2.5 Performance Profiling 
**n/a**

#### 5.2.6 Load Testing
**n/a**

#### 5.2.7 Stress Testing
**n/a**

#### 5.2.8	Volume Testing
**n/a**

#### 5.2.9	Security and Access Control Testing
**n/a**

#### 5.2.10	Failover and Recovery Testing
**n/a**

#### 5.2.11	Configuration Testing
**n/a**

#### 5.2.12	Installation Testing
**n/a**

## 6.	Entry and Exit Criteria
### 6.1	Test Plan
#### 6.1.1	Test Plan Entry Criteria
A version of Itemize will be build on an standard Android Emulator device, which equals a clean install of stock android.
Also it will be tested on an outdated Android version on a custom build from Huawei (Huawei P8 lite, Android Marshmallow 6.0, EMUI 4.0.3).
Additionally we test on a device with a large screen size and the latest Android Version available (March 2020) from Oneplus (Oneplus 6, Android Q 10, 6,28")

#### 6.1.2	Test Plan Exit Criteria
When all tests pass without throwing an exception.
#### 6.1.3 Suspension and Resumption Criteria
n/a

## 7.	Deliverables
### 7.1	Test Evaluation Summaries
The test run summary will be available in our repository, when it is finished
### 7.2	Reporting on Test Coverage
n/a
### 7.3	Perceived Quality Reports
n/a
### 7.4	Incident Logs and Change Requests
n/a
### 7.5	Smoke Test Suite and Supporting Test Scripts
n/a
### 7.6	Additional Work Products
#### 7.6.1	Detailed Test Results
The detailed test results will be available in our repository, when it is finished

#### 7.6.2	Additional Automated Functional Test Scripts
n/a
#### 7.6.3	Test Guidelines
n/a
#### 7.6.4	Traceability Matrices
n/a

## 8.	Testing Workflow
Developers should execute tests locally before pushing source code. When pushing to master, tests are executed automatically, when pipelining is implemented.
## 9.	Environmental Needs
This section presents the non-human resources required for the Test Plan.
### 9.1	Base System Hardware
n/a

### 9.2	Base Software Elements in the Test Environment
n/a

### 9.3	Productivity and Support Tools
n/a

### 9.4	Test Environment Configurations
n/a

## 10.	Responsibilities, Staffing, and Training Needs
### 10.1	People and Roles
This table shows the staffing assumptions for the test effort.

Human Resources

| Role | Minimum Resources Recommended (number of full-time roles allocated) |	Specific Responsibilities or Comments |
|---|---|---|
| Test Designer | 1 | Defines the technical approach to the implementation of the test effort. <br> Responsibilities include:<br> define test approach<br> define test automation architecture<br> verify test techniques<br> define testability elements<br> structure test implementation|
| Tester | 1 |	Implements and executes the tests.<br> Responsibilities include:<br> implement tests and test suites<br> execute test suites<br> log results<br> analyze and recover from test failures<br> document incidents|
| Implementer | 3 | Implements and unit tests the test classes and test packages.<br> Responsibilities include:<br> creates the test components required to support testability requirements as defined by the designer |

### 10.2	Staffing and Training Needs
**n/a**
## 11.	Iteration Milestones

n/a

## 12.	Risks, Dependencies, Assumptions, and Constraints
| Risk | Mitigation Strategy	| Contingency (Risk is realized) |
|---|---|---|
| Unexpected failures | Cover all important functions in tests | Rollback deployment to last stable version |
## 13. Management Process and Procedures

### 13.1 Deployment process
A pipeline triggers, when pushing a new version to the gitlab repository and is doing some jobs for us.
- It builds the application in an docker container and makes sure, that everything works as intented
- It tests the application and denys the push, if one of the tests is failing
- Codacy will be triggered and investigates the code for new issues or bad coding style