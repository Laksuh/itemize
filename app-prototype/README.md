# Installation 
To install the app, you need to allow the installation form unknown sources, because we have not registered ourselfs or the app in the google play store and google is not aware of our existence yet.

### Installtion Guide
if you need help, checkout this installation guide: 

https://itemizeproject.wordpress.com/2020/06/09/week-9-have-a-look-at-our-app/

### User Guide
https://itemizeproject.wordpress.com/user-guide/

### Patch notes
https://itemizeproject.wordpress.com/patch-notes/
