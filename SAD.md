# Software Architecture Document

#### Table of Contents
- 1.	Introduction	
- 1.1	Purpose	
- 1.2	Scope
- 1.3	Definitions, Acronyms, and Abbreviations	
- 1.4	References	
- 1.5	Overview	
- 2.	Architectural Representation    
- 2.1   Patterns
- 3.	Architectural Goals and Constraints	
- 4.	Use-Case View	
- 4.1	Use-Case Realizations	
- 5.	Logical View
- 5.2	Architecturally Significant Design Packages	
- 6.	Process View	
- 7.	Deployment View	
- 8.	Implementation View	
- 8.1	Overview	
- 8.2	Layers	
- 9.	Data View (optional)	
- 10.	Size and Performance	
- 11.	Quality	


### Software Architecture Document

##### 1.	Introduction
###### 1.1	Purpose
This document provides a comprehensive architectural overview of the system, using a number of different architectural views to depict different aspects of the system. It is intended to capture and convey the significant architectural decisions which have been made on the system.
REMINDER: (Hyper)Links will follow soon.
###### 1.2	Scope
The scope of this SAD is to show the architecture of our Itemize project. Illustrated are the Use-Cases, the class and data structure.
###### 1.3	Definitions, Acronyms, and Abbreviations
| Abbrevation | Description                         |
| ----------- | ----------------------------------- |
| API         | Application programming interface   |
| MVC         | Model View Controller               |
| SDK         | Software development kit            |
| SRS         | Software Requirements Specification |
| UC          | Use Case                            |
| n/a         | not applicable                      |
| tbd         | to be determined                    |
###### 1.4	References /tbd)

###### 1.5	Overview
This document contains the Architectural Representation, Goals and Constraints as well as the Logical, Deployment, Implementation and Data Views.
##### 2.	Architectural Representation 
The app is completely coded in Java, XML and other required languages/patterns for our Android Studio IDE (f.e. gradle) for now. 

With XML all Views are created and controlled by their belonging Activity.

Our tests are made with Espresso, which is the usual way to test in Android. 

We are not aware of a database for now, so if we decided to add one later, this and all dependencies will be added here later.

###### 2.1 Patterns
In our app we used the Adapter pattern which is native for android programming.
The idea behind the pattern is to handle requests by an adapter, which can handle actions for an amount of objects of the same type for each object on its own, instead of personally differentiate them by yourself with additional code.
On this way, you just need one adapter to do actions on each object with interface, instead of handling every action of each object on its own.

##### 3.	Architectural Goals and Constraints 

Our Goal is it to follow the MVC pattern in our application. Luckily Android provides this nearly automatically and we don't need to account or force this.

Another goal is to follow Androids design guidelines, because we want our users to comfortably navigate through our app.

##### 4.	Use-Case View 

![UCD][logo]

[logo]: https://gitlab.com/Laksuh/itemize/raw/master/UCD.png "UCD"

###### 4.1	Use-Case Realizations

We were able to realize all planned Use-Cases in the given time. The expansion as seen in the overview could not be made.

##### 5.	Logical View 

Our Project for now looks like this

![CD][classd]

[classd]: https://itemizeproject.files.wordpress.com/2019/11/image-16.png?w=1024 "CD"

The project plays around the 2 following classes:
Item and Location.
Item is the class for the dragable, editable and seen items on the users screen, with attributes like their name and their image.
Location is the class for the editable Locations, which are basically just an attribute for the item (in which location it is stored at the moment).

All other classes are just realizations of the UI and functionality o this concept. The Adapters (ItemAdapter or LocationAdapter) are just managing the items/locations in the application. The Adapters are able to add/delete items in their saved document and display them in real time on the users screen.

##### 6.	Process View 
n/a
##### 7.	Deployment View 
The app runs on the users devices. It does not calculate on some servers for the moment and the fluentness of the application depends on the users device power, but it was tested on pretty low standards (Huawei P8 lite with 8x1,2GHz and 2Gb of slow RAM, little available storage; also the emulator (even worse)) and on powerful ones (Oneplus 6  4x2.8 and 4x 1.77GHz, 8GB of fast RAM, enough available storage) and worked both times nearly lagg-free.               
##### 8.	Implementation View 
n/a
###### 8.1	Overview
n/a
###### 8.2	Layers
n/a
##### 9.	Data View (optional)
The status for now is, that we are not having a database.
##### 10.	Size and Performance 
n/a
##### 11.	Quality (tbd)
To ensure quality, we added UI-tests with Espresso 
f.e. 
![test][test]

[test]: https://itemizeproject.files.wordpress.com/2019/12/additemandlocationtest.gif "test"